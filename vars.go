package main

import (
	"fmt"
	"os"

	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
)

var (
	env = os.Getenv("PULUMI_STACK")

	//Labels to attach to created resources
	labels = pulumi.StringMap{
		"env":   pulumi.String(env),
		"stack": pulumi.String("gitlab-omnibus"),
	}

	//compute instance settings
	computeInstance = struct {
		name     string
		size     string
		zone     string
		image    string
		svcEmail string
		labels   pulumi.StringMap
	}{
		name:     fmt.Sprintf("gitlab-%s", env),
		size:     "e2-medium",
		zone:     "us-central1-a",
		image:    "ubuntu-2004-focal-v20200917",
		svcEmail: os.Getenv("SVC_ACCOUNT_EMAIL"),
		labels:   labels,
	}

	//Network settings
	instanceNetwork = struct {
		name        string
		autoSubnets bool
		labels      pulumi.StringMap
	}{
		name:        fmt.Sprintf("gitlab-%s-net", env),
		autoSubnets: true,
		labels:      labels,
	}

	//Firewall settings
	instanceFirewall = struct {
		name   string
		labels pulumi.StringMap
		ports  pulumi.StringArray
	}{
		name:   fmt.Sprintf("gitlab-%s-fw", env),
		labels: labels,
		ports: pulumi.StringArray{
			pulumi.String("22"),
			pulumi.String("80"),
			pulumi.String("443"),
		},
	}

	//Address settings
	instanceAddr = struct {
		name        string
		addressType string
		region      string
		labels      pulumi.StringMap
	}{
		name:        fmt.Sprintf("gitlab-%s-addr", env),
		addressType: "EXTERNAL",
		region:      "us-central1",
		labels:      labels,
	}

	//Cloudflare DNS Record
	record = struct {
		name      string
		subdomain string
		zone      string
		kind      string
		proxied   bool
	}{
		name:      fmt.Sprintf("gitlab-%s-record", env),
		subdomain: fmt.Sprintf("gitlab-%s", env),
		zone:      os.Getenv("CLOUDFLARE_ZONE_ID"),
		kind:      "A",
		proxied:   true,
	}
)

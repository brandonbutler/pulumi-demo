module gitlab-proj

go 1.14

require (
	github.com/pulumi/pulumi-cloudflare/sdk/v2 v2.6.0
	github.com/pulumi/pulumi-gcp/sdk/v3 v3.25.0
	github.com/pulumi/pulumi/sdk/v2 v2.11.2
)

# Pulumi Demo

## Purpose

To try out Pulumi while installing GitLab on some GCP infrastructure

## Stack

* Pulumi (IaC to GCP and Cloudflare)
* Ansible (Playbook to install GitLab Omnibus)

## Supporting Information

IaC was written using a tool called "Pulumi." Pulumi is interesting because it allows you to write IaC in 
a general purpose programming language like Golang instead of using a domain specific language (DSL) such as
Terraform's "HCL."

The IaC will provision in GCP:
 * A compute instance with the minimal hardware specifications required for GitLab Omnibus
 * A public IP address to attach to the instance
 * A firewall that allows ICMP, and TCP ports 22/80/443
 * A non-default network

The IaC will also provision one DNS record in CloudFlare for the subdomain `https://gitlab.brandonbutle.red`
which will point at the public IP address that was provisioned.

Once Pulumi has finished provisioning the instances, Ansible will kick off to wait for the GCP instance to 
become reachable, and once it is, begin installing GitLab Omnibus in the methods prescribed by: 
 * https://about.gitlab.com/install/#ubuntu

The playbook uses a heavily simplified version of the geerlingguy role available here: 
 * https://github.com/geerlingguy/ansible-role-gitlab

## To Destroy

Simply run the `when: manual` job to destroy the deployment once done.
package main

import (
	"fmt"

	"github.com/pulumi/pulumi-cloudflare/sdk/v2/go/cloudflare"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/compute"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		//Create network
		network, err := compute.NewNetwork(ctx, instanceNetwork.name, &compute.NetworkArgs{
			Name:                  pulumi.String(instanceNetwork.name),
			AutoCreateSubnetworks: pulumi.Bool(instanceNetwork.autoSubnets),
		})
		if err != nil {
			return fmt.Errorf("[ERROR] Creating new network: %v", err)
		}

		//Create firewall that allows 22/80/443
		firewall, err := compute.NewFirewall(ctx, instanceFirewall.name,
			&compute.FirewallArgs{
				Network: network.SelfLink,
				Allows: &compute.FirewallAllowArray{
					&compute.FirewallAllowArgs{
						Protocol: pulumi.String("tcp"),
						Ports:    instanceFirewall.ports,
					},
					&compute.FirewallAllowArgs{
						Protocol: pulumi.String("icmp"),
					},
				},
			},

			//Resource depends
			pulumi.DependsOn([]pulumi.Resource{network}),
		)
		if err != nil {
			return fmt.Errorf("[ERROR] Creating new firewall: %v", err)
		}

		//Create external IP addr
		address, err := compute.NewAddress(ctx, instanceAddr.name,
			&compute.AddressArgs{
				Labels:      instanceAddr.labels,
				AddressType: pulumi.String(instanceAddr.addressType),
				Region:      pulumi.String(instanceAddr.region),
			},
		)
		if err != nil {
			return fmt.Errorf("[ERROR] Creating new address: %v", err)
		}

		//Create compute instance
		instance, err := compute.NewInstance(ctx, computeInstance.name,
			&compute.InstanceArgs{
				//Boot disk image specification
				BootDisk: &compute.InstanceBootDiskArgs{
					InitializeParams: &compute.InstanceBootDiskInitializeParamsArgs{
						Image: pulumi.String(computeInstance.image),
					},
				},

				//Networking specs
				NetworkInterfaces: &compute.InstanceNetworkInterfaceArray{
					&compute.InstanceNetworkInterfaceArgs{
						Network: network.ID(),
						AccessConfigs: &compute.InstanceNetworkInterfaceAccessConfigArray{
							&compute.InstanceNetworkInterfaceAccessConfigArgs{
								NatIp: address.Address,
							},
						},
					},
				},

				//Instance settings
				MachineType: pulumi.String(computeInstance.size),
				Zone:        pulumi.String(computeInstance.zone),
				Labels:      labels,

				//Service account attachment
				ServiceAccount: &compute.InstanceServiceAccountArgs{
					Scopes: pulumi.StringArray{
						pulumi.String("https://www.googleapis.com/auth/cloud-platform"),
					},
					Email: pulumi.String(computeInstance.svcEmail),
				},
			},

			//Resource depends
			pulumi.DependsOn([]pulumi.Resource{firewall, network, address}),
		)
		if err != nil {
			return fmt.Errorf("[ERROR] Creating new compute instance: %v", err)
		}

		//Cloudflare configuration (Point DNS to new Instance IP)
		_, err = cloudflare.NewRecord(ctx, record.name, &cloudflare.RecordArgs{
			ZoneId:  pulumi.String(record.zone),
			Name:    pulumi.String(record.subdomain),
			Value:   address.Address,
			Type:    pulumi.String(record.kind),
			Proxied: pulumi.Bool(record.proxied),
		},
			//Resource depends
			pulumi.DependsOn([]pulumi.Resource{address}),
		)
		if err != nil {
			return fmt.Errorf("[ERROR] Updating Cloudflare DNS record: %v", err)
		}

		//Exports
		ctx.Export("instanceName", instance.Name)
		ctx.Export("externalIP", address.Address)
		return nil
	})
}
